from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *

spark = SparkSession.builder\
        .appName("twitter_kafka_spark_consumer")\
        .master("local[*]")\
        .config("spark.sql.shuffle.partitions","2")\
        .getOrCreate()

data = spark.readStream\
        .format("kafka")\
        .option("kafka.bootstrap.servers", "localhost:9092")\
        .option("subscribe", "maut")\
        .load()

processing = data.selectExpr("CAST(value AS STRING) as val")\
        .select(from_json("val", "id STRING, time STRING, text STRING").alias("x"))\
        .selectExpr("CAST(from_unixtime(x.time,'HH:mm:ss') as TIMESTAMP) as time" ,"explode(split(lower(x.text),'[^#a-z]')) as line")\
        .where("substring(line,1,1) = '#'")\
        .where("length(line) > 1")\
        .groupBy("line", window("time",windowDuration='60 minute')).agg(count("line").alias("count"))\
        .selectExpr("(line, count, window.start as start,window.end as end) as result") \
        .select(to_json("result").alias("value"))

processing.writeStream\
        .format("console")\
        .option("truncate",False)\
        .outputMode("update")\
        .start()

processing.writeStream\
        .format("kafka")\
        .outputMode("complete") \
        .option("kafka.bootstrap.servers", "localhost:9092") \
        .option("topic", "staged_data")\
        .option("checkpointLocation", "/home/sunbeam/checkpoint/")\
        .start()

spark.streams.awaitAnyTermination()

spark.stop()