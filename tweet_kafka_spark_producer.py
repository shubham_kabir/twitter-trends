import tweepy
import json
import os
from time import sleep
import json
from kafka import KafkaProducer
import random
from datetime import datetime

producer = KafkaProducer(bootstrap_servers=['localhost:9092'], value_serializer=lambda x: json.dumps(x).encode('utf-8'))
print("producer ready.")

class MyStreamListener(tweepy.StreamListener):
    def on_error(self, status_code):
        if status_code == 420:
            print("------------ limit exceeds ------------")
            return False

    def on_status(self, status):
        tweet = dict()
        tweet['id'] = status.id_str
        tweet['time'] = status.timestamp_ms
        tweet_text = ""
        try:
            tweet_text = status.extended_tweet.full_text
        except:
            tweet_text = status.text
        tweet['text'] = tweet_text
        if len(tweet_text) > 0:
            file_path = "/home/sunbeam/tweets_json/" + status.id_str + ".json"
            file = open(file_path, 'w')
            file.write(json.dumps(tweet))
            file.close()
            # print tweet on terminal
            print(tweet)
            producer.send(topic = 'maut', value=tweet)



if __name__ == '__main__':
    consumer_key = "y234z78fsdghfjSJ360B"
    consumer_secret = "1231234qDP92LNZc7NhQld99pi0iVnTFFYk9x"
    access_token = "qwrefgrgdjfykulq0qy9SCrewthfyju"
    access_token_secret = "beq3wter5uC8DIrwasdhfjgkuhluQ6wafetsrytykulP"

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)

    myStreamListener = MyStreamListener()
    myStream = tweepy.Stream(auth=api.auth, listener=myStreamListener)

    myStream.filter(track=['the', 'i', 'to', 'a', 'and', 'is', 'in', 'it', '&'], languages=['en'])
